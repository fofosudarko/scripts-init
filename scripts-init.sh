#!/bin/sh -e
#
# File: scripts-init.sh - prepend shebang to script file
#
# (c) 2019 Frederick Ofosu-Darko <fofosudarko@gmail.com>
#
# Usage: sh scripts-init.sh FILENAME SHEBANG PURPOSE USAGE
#
#

## - subroutines

usage () {
	cat <<eof
Usage: ${0} AUTHOR FILENAME SHEBANG PURPOSE USAGE
Ex:    ${0} "Frederick Ofosu-Darko <fofosudarko@gmail.com>" \
hello.sh "/bin/sh" "prints 'Hello, World!' to stdout" "sh hello.sh"

eof
	exit "${1}";
}

## -- check number of CLI arguments

if [ $# -ne 5 ]; then
	echo >&2 "Error: ${0} expects exactly five arguments!!!";
	usage 2;
fi

## -- arguments

author="${1}";
filename="${2}";
shebang="${3}";
purpose="${4}";
_usage="${5}";

cat 1> "${filename}" <<eof
#!${shebang}
#
# File: ${filename} -> ${purpose}
#
# (c) $(date +%Y) ${author}
#
# Usage: ${_usage}
#
#

## - start here

exit 0

## -- finish
eof

exit 0;

## -- finish
